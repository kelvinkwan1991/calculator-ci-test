import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CalculatorTest {
    private Calculator calculator = new SimpleCalculator();

    @Test
    void add() {
        // given
        int a = 10, b = 20;
        // when
        int result = calculator.add(a, b);
        // then
        assertThat(result).isEqualTo(30);
    }

    @Test
    void subtract() {
        int a = 20, b = 10;
        int result = calculator.subtract(20, 10);
        assertThat(result).isEqualTo(10);
    }
}